<?php 
  session_start();
  if(!isset($_SESSION['data'])) {
    header('Location: index.php');
  } 
  $data = $_SESSION['data'];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Hitung Nilai</title>
	<link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>

	<div class="card">
    <a href="functions/clear.php">Back to Home</a>
      <p>Hasil</p>
      <ul>
        <li>Nama: <?= $data['nama_siswa'] ?>  </li>
        <li>Nilai: <?= $data['nilai'] ?></li>
        <li>Rata-rata: <?= $data['rata_rata'] ?></li>
        <li>Predikat: <?= $data['predikat'] ?> </li>  
      </ul>
      
    </div>

</body>
</html>