<?php

if(isset($_POST["nama"]) && isset($_POST["nilai"]) && is_array($_POST["nilai"])){
    $nama = $_POST["nama"];
    $nilai = $_POST["nilai"];
    $rata_rata = array_sum($nilai) / count($nilai);
    $stringNilai = implode(", ", $nilai);
    
    foreach ($nilai as $num) {
        if (!preg_match("/^[0-9]+$/", $num) ) {  
            $ErrMsg = "Only numeric value is allowed.";
            echo $ErrMsg;
            exit;
        }

        if($num > 100) {
            $ErrMsg = "Max value is 100.";
            echo $ErrMsg;
            exit;
        }
    }
    
    if (!preg_match ("/^[a-zA-z ]*$/", $nama) ) {  
        $ErrMsg = "Only alphabets are allowed.";
        echo $ErrMsg;
    } else {  
        if($rata_rata >= 80) {
            $predikat = 'A';
        } else if ($rata_rata >= 60) {
            $predikat = 'B';
        } else if ($rata_rata >= 40) {
            $predikat = 'C';
        } else {
            $predikat = 'D';
        }
        
        foreach($nilai as $key => $value){
            $data = [
                'nama_siswa' => $nama,
                'nilai' => $stringNilai,
                'rata_rata' => $rata_rata,
                'predikat' => $predikat
            ];
        }

        session_start();
        $_SESSION['data'] = $data;

        header('Location: ../result.php');
    }
}